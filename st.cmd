require essioc
require xtpico,0.12.0+0

epicsEnvSet("PREFIX",    "MO")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("DEVICE_IP", "172.16.100.214") #rf-mo-01.tn.esss.lu.se
epicsEnvSet("I2C_COMM_PORT", "AK_I2C_COMM")

epicsEnvSet("R_DU",  ":RFS-DU-001:")
epicsEnvSet("R_DRO", ":RFS-DRO-001:")
epicsEnvSet("R_XTPICO", ":Ctrl-IM-001:")

epicsEnvSet("R_LTC2991_1",  "$(R_XTPICO)VMon1")
epicsEnvSet("R_LTC2991_2",  "$(R_XTPICO)VMon2")
epicsEnvSet("R_LTC2991_3",  "$(R_XTPICO)VMon3")
epicsEnvSet("R_TMP100_1",   "$(R_XTPICO)Temp1")
epicsEnvSet("R_TMP100_2",   "$(R_XTPICO)Temp2")
epicsEnvSet("R_M24M02",     "$(R_XTPICO)Eeprom")
epicsEnvSet("R_TCA9555_1",  "$(R_XTPICO)IOExp")

epicsEnvSet("RF_THOLD1",  "20")
epicsEnvSet("RF_THOLD2",  "20")
epicsEnvSet("PLL_THOLD",  "2")
epicsEnvSet("PLL_ERR_TIMEOUT",  "2")

iocshLoad("iocsh/mo.iocsh")

## Add any post-iocInit statements here
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_RFin-Factor-S' 47.62")
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_704-Factor-S' 47.62")
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_352-Factor-S' 47.62")
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_88-Factor-S' 47.62")
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_PVin-Factor-S' 1.5")
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_P5VRF-Factor-S' 2")
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_P5VD-Factor-S' 2")
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_P3V3D-Factor-S' 2")
afterInit("dbpf '$(PREFIX)$(R_DRO)P5V-Factor-S' 1.546")
afterInit("dbpf '$(PREFIX)$(R_DRO)P12V-Factor-S' 4.727")

afterInit("dbpf '$(PREFIX)$(R_DU)Mon_RFin-Offset-S' -21")
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_704-Offset-S' -29")
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_352-Offset-S' -29")
afterInit("dbpf '$(PREFIX)$(R_DU)Mon_88-Offset-S' -29")

afterInit("dbpf '$(PREFIX)$(R_TCA9555_1)-DirPin5' 'Output'")
afterInit("dbpf '$(PREFIX)$(R_TCA9555_1)-DirPin6' 'Output'")
afterInit("dbpf '$(PREFIX)$(R_TCA9555_1)-DirPin7' 'Output'")

iocInit()
